/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Alert,
  TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Content from './components/Content/Content';
import { useState } from 'react';

  
const App: () => React$Node = () => {
  const [text, setText] = useState('');

  validateEmail = () => {
    if(text==='') {
      alert("Please enter an email to test")
    }
    else {
      if(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(text))
        Alert.alert("Validation Result",`"${text}"` + ' is a valid email.');
      else
        Alert.alert("Validation Result",`"${text}"` + ' is not a valid email!');
}
    }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}

          <View style={styles.sectionContainer}>
            <Text style={styles.sectionTitle}>React Native Assignment</Text>
          </View>

          <View style={styles.textContainer}>
            <TextInput style={styles.textInput} placeholder="Enter email" onChangeText={text => setText(text)} defaultValue={text} />
            <TouchableOpacity style={styles.buttonContainer} onPress={() => validateEmail()}>
              <Text style={styles.buttonText}>Test</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.white,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: 'white',
  },
  sectionContainer: {
    backgroundColor:'#05A081'
  },
  sectionTitle: {
    textAlign:'center',
    padding:20,
    fontSize: 32,
    fontWeight: '800',
    color: Colors.black,
  },
  textContainer: {
    flex:1,
    paddingTop:'50%',
    paddingBottom:'70%',
    backgroundColor:'#eee',
  },
  textInput: {
    fontSize:20,
    backgroundColor:Colors.white,
    borderWidth:2,
    borderColor:'#000',
    margin:'10%',
  }, 
  buttonContainer: {
    flex:1,
    alignSelf:'center',
    color:Colors.white,
    backgroundColor: '#000',
    width:'30%',
    height:45,
    borderRadius:5
  },
  buttonText: {
    color:Colors.white,
    fontSize:24,
    textAlign:'center',
    padding:5
  }
});

export default App;
